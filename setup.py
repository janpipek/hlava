#!/usr/bin/env python
from setuptools import setup, find_packages
import hlava

setup(
    name=hlava.__name__,
    version=hlava.__version__,
    packages=find_packages(),
    license='MIT',
    description='Simple open-source DICOM browser/viewer in Python and Qt4.',
    long_description=open('README.md').read(),
    author=hlava.__author__,
    author_email='jan.pipek@gmail.com',
    url='https://github.com/janpipek/hlava',
    install_requires = [ 'appdirs', 'six', 'blinker', 'qtawesome' ],
    entry_points = {
        'console_scripts' : [
            'hlava = hlava.cli:console_app'
        ],
        'gui_scripts' : [
            'hlava-qt = hlava.qtui.app:run_app'
        ]
    }
)
