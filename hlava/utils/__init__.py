import os


class CannotMkdirError(RuntimeError):
    pass


def ensure_directory_existence(path):
    """Ensure that a directory exists.

    Creates it if necessary"""
    if not os.path.isdir(path):
        if os.path.exists(path):
            raise CannotMkdirError("Cannot create directory. Another entity with that name exists.")
        else:
            os.makedirs(path)
    if not os.path.isdir(path):
        raise CannotMkdirError("Problem when creating directory.")
