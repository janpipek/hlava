import git

import logging
logger = logging.getLogger(__name__)


class Versioning(object):
    def __init__(self, storage):
        """

        :type storage: hlava.filestorage.FileStorage
        :param storage:
        :return:
        """
        self.storage = storage
        self.repo = git.Repo(storage.root_path)
        self.storage.on_operation_executed.connect(self._on_operation, sender=self.storage)

    def _reset(self):
        self.repo.git.reset()

    def _on_operation(self, sender, operation):
        """

        :param operation:
        :type operation: hlava.operations.Operation
        :return:
        """
        added = operation.files_added
        updated = operation.files_updated
        removed = operation.files_removed
        renamed = operation.files_renamed

        logger.debug("Committing to git...")

        self.repo.git.reset()
        for f in operation.files_added:
            self.repo.git.add(f)
        for f in operation.files_updated:
            self.repo.git.add(f)
        for old_f, new_f in operation.files_renamed.items():
            self.repo.git.rm(old_f)
            self.repo.git.add(new_f)
        for f in operation.files_removed:
            self.repo.git.rm(f)

        message = operation.short_description + "\n\n" + operation.full_description
        self.repo.git.commit("-m", message)
        logger.info("New commit created.")

    def item_change(self, name, path):
        self._reset()
        self.repo.git.add(path)
        self.repo.git.commit("-m", "Change: %s" % name)

    def item_create(self, name, path):
        self._reset()
        self.repo.git.add(path)
        self.repo.git.commit("-m", "Create: %s" % name)