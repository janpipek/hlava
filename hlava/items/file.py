import codecs
import os
import shutil
from .item import Item

class FileItem(Item):
    """Abstract file item.

    All items that correspond to a file or directory should inherit from this."""

    @classmethod
    def applies(cls, path, **kwargs):
        return False

    def __init__(self, id, storage, path, **kwargs):
        super(FileItem, self).__init__(id, storage, **kwargs)
        self.path = path
        if "__on_init__" in dir(self):
            self.__on_init__()

    def _repr_text(self):
        with codecs.open(self.path, mode="r", encoding="utf-8") as f:
            return f.read()

    def _set_text(self, value):
        with codecs.open(self.path, mode="w", encoding="utf-8") as f:
            return f.write(value)

    def change_to_directory(self):
        new_dir = os.path.join(os.path.dirname(self.path), self.basename)
        if os.path.exists(new_dir):
            if not os.path.isdir(new_dir):
                raise Exception("Cannot change to directory.")
        else:
            os.makedirs(new_dir)
        from .directory import DirectoryItem
        new_path = os.path.join(new_dir, "%s.%s" % ("_main", self.extension))
        shutil.move(self.path, new_path)
        return DirectoryItem(self.name, self.storage, new_dir)

    def create_child(self, name, extension="md", **kwargs):
        dir_item = self.change_to_directory()
        item = dir_item.create_child(name, extension=extension, **kwargs)
        return item

    @property
    def extension(self):
        return os.path.splitext(self.path)[1][1:]