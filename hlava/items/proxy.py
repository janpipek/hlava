

class ProxyItemMixin(object):
    """A proxy behaviour for items that don't hold data themselves."""

    @property
    def inner(self):
        return None

    def __getattr__(self, name):
        if self.inner:
            return getattr(self.inner, name)
        raise AttributeError("%r object has no attribute %r" % (self.__class__, name))