from .item import Item
from .proxy import ProxyItemMixin
from . import register, find_constructor
from ..filestorage import valid_basename, InvalidNameError
import os


@register
class DirectoryItem(Item, ProxyItemMixin):
    """Directory item."""

    extension = ""

    @classmethod
    def applies(cls, path, **kwargs):
        return os.path.isdir(path)

    def __init__(self, name, storage, path, **kwargs):
        super(DirectoryItem, self).__init__(name, storage, **kwargs)
        self.path = path
        self._inner = self.storage.get_item("%s._main" % self.name, throw=False, enable_meta=True)

    @property
    def editable(self):
        return True

    def _set_text(self, value):
        inner = self.inner
        if inner:
            inner.text = value
        else:
            self._inner = self.create_child("_main", "md", content=value, enable_meta=True)

    @property
    def inner(self):
        return self._inner

    def has_children(self):
        return True

    def get_children_names(self, **kwargs):
        files = sorted(os.listdir(self.path))
        any_type = kwargs.get("any_type")

        children = set()
        for f in files:
            path = os.path.join(self.path, f)
            if os.path.isdir(path):
                if valid_basename(f):
                    children.add(f)
            elif os.path.isfile(path):
                name, _ext = os.path.splitext(f)
                if valid_basename(name):
                    if any_type or find_constructor(path):
                        children.add(name)
        return sorted(list(children))

    def get_child(self, name, **kwargs):
        if not valid_basename(name):
            raise InvalidNameError("Child name must be a single part of path.")
        new_name = self._get_child_name(name)
        return self.storage.get_item(new_name)

    def create_child(self, name, extension, **kwargs):
        new_name = "%s.%s" % (self.name, name)
        return self.storage.create_item(new_name, extension, **kwargs)

    def change_to_directory(self):
        pass # Already is

    def _repr_html_(self):
        """HTML representation of item

        :rtype: unicode
        """
        html = u""
        if self.inner:
            html += self.inner._repr_html_()
        if self.has_children():
            children = self.get_children_names()
            if children:
                html += "<h2>Contents</h2>"
                html += "<ul>"
                for child in children:
                    html += "<li><a href='hlava://%s'>%s</a></li>" % (self._get_child_name(child), child)
                html += "</ul>"
        return html

class RootItem(DirectoryItem):
    """Top-level directory item."""

    def __init__(self, storage, **kwargs):
        self.name = ""
        self.path = storage.root_path
        super(RootItem, self).__init__(self.name, storage, self.path, **kwargs)

    def _get_child_name(self, name):
        return name

    @property
    def title(self):
        return "[hlava]"