class ItemNotEditableError(RuntimeError):
    pass


class Item(object):
    """Abstract item of the system."""

    def __init__(self, name, storage, **kwargs):
        self.storage = storage
        self.name = name
        # self.inner = None

    @classmethod
    def create(cls, storage, name):
        return storage.create_item(name, cls.extension)

    @property
    def text(self):
        try:
            return self._repr_text()
        except AttributeError:
            return u""

    def create_child(self, name, **kwargs):
        raise NotImplementedError()

    @property
    def editable(self):
        return False

    @text.setter
    def text(self, value):
        if not self.editable:
            raise ItemNotEditableError()
        else:
            self._set_text(value)

    @property
    def basename(self):
        """The last part of the item name."""
        return self.name.split(".")[-1]

    @property
    def parent_name(self):
        if self.name:
            return ".".join(self.name.split(".")[:-1])
        else:
            return None

    @property
    def parent_item(self):
        name = self.parent_name
        if name:
            return self.storage.get_item(name)
        else:
            return None

    @property
    def title(self):
        return self.name

    def has_children(self):
        return False

    def get_children_names(self):
        return []

    def get_child(self, name):
        return None

    def _get_child_name(self, name):
        return "%s.%s" % (self.name, name)

    def change_to_directory(self):
        raise NotImplementedError()

    @property
    def mime_type(self):
        return None
