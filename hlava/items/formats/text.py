from ..file import FileItem
from .. import register
import os
import mimetypes

@register
class TextItem(FileItem):
    extension = "txt"

    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext in ["txt"]:
                return 1.0
            mime_candidate = mimetypes.guess_type(path)
            if mime_candidate[0] and mime_candidate[0].startswith("text/"):
                return 0.5
        return False

    def append(self, text):
        if not text.endswith("\n"):
            text += "\n"
        with open(self.path, "a") as f:
            f.write(text)

    @property
    def editable(self):
        return True

    def _repr_html_(self):
        return "<pre>%s</pre>" % self.text

    @property
    def mime_type(self):
        return "text/plain"