from ..file import FileItem
from .. import register
import os


@register
class ExcelItem(FileItem):
    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext in ["xls", "xlsx", "xlsm"]:
                return True
        return False

    def _repr_html_(self):
        ext = os.path.splitext(self.path)[1][1:]
        if ext == "xls":
            import xlrd
            return "Old XLS files almost implemented."
        else:
            import openpyxl
            workbook = openpyxl.load_workbook(self.path, data_only=True)
            html = ""
            for n in workbook.get_sheet_names():
                worksheet = workbook.get_sheet_by_name(n)
                dims = get_worksheet_dimensions(worksheet)
                html += "<h2>%s</h2>" % n
                html += "<table border='1'>"
                for row in xrange(dims[0]):
                    html += "<tr>"
                    for column in xrange(dims[1]):
                        cell = worksheet.cell(row=row+1, column=column+1)
                        style = ""
                        if cell.has_style:
                            cell_style = cell.style
                            if cell_style.font:
                                if cell_style.font.b:
                                    style += "font-weight:bold;"
                                if cell_style.font.sz:
                                    style += "font-size:%fpt;" % cell_style.font.sz
                                if cell_style.font.name:
                                    style += "font-family:'%s';" % cell_style.font.name
                            # Parse the style
                        value = cell.value or "&nbsp;"
                        html += "<td style=\"%s\">%s</td>" % (style, value)
                    html += "</tr>"
                html += "</table>"
        return html

def get_worksheet_dimensions(worksheet):
    """Find real worksheet dimensions.

    :type worksheet: openpyxl.worksheet.Worksheet
    :param worksheet:
    :return: a tuple of (max_row, max_column)
    :rtype: tuple

    The dimensions reported by the worksheet itself are too large.
    """
    from openpyxl.cell import column_index_from_string
    max_row = 1
    max_column = 1
    for row in worksheet.rows:
        for cell in row:
            if cell.value is not None:
                max_row = max(max_row, cell.row)
                max_column = max(max_column, column_index_from_string(cell.column))
    return max_row, max_column