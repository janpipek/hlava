from .. import register
from .text import TextItem

import os

from docutils.core import publish_string

@register
class RstItem(TextItem):
    extension = "rst"

    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext == "rst":
                return True
        return False

    def _repr_html_(self):
        html = publish_string(self.text, writer_name="html",
                              settings_overrides={'input_encoding': 'unicode',
                                                  'report_level': 4,
                                                  'math_output': 'MathJax'})
        return html.decode('utf-8')

    @property
    def mime_type(self):
        return "text/x-rst"