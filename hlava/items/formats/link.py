from ..file import FileItem
from ..proxy import ProxyItemMixin
from .. import register
import os


class InvalidLinkError(RuntimeError):
    pass


@register
class LinkItem(FileItem, ProxyItemMixin):
    """A link item that points to another item."""

    extension = "link"

    @classmethod
    def applies(cls, path):
        if not os.path.isfile(path):
            return False
        return os.path.splitext(path)[1] == ".link"

    def __on_init__(self):
        with open(self.path, "r") as f:
            self.link = f.read().strip()
            self._inner = None

    @property
    def inner(self):
        if not self._inner:
            try:
                self._inner = self.storage.get_item(self.link)
            except:
                raise InvalidLinkError("Link leads to non-existent page: %s" % self.link)
        return self._inner

    def _repr_html_(self):
        html = u"<div class='link-info'>Linked from: %s</div>" % self.name
        try:
            html += self.inner._repr_html_()
        except InvalidLinkError as err:
            html += "<div class='error'>%s</div>" % err.message
        return html

    def get_child(self, name):
        return self.inner.get_child(name)

    def get_children_names(self):
        return self.inner.get_children_names()