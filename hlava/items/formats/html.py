import os

from .. import register
from .text import TextItem


@register
class HtmlItem(TextItem):
    extension = "html"

    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext in ["html", "htm"]:
                return True
        return False

    def _repr_html_(self):
        return self.text

    @property
    def mime_type(self):
        return "text/html"