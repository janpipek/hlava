import os

from .. import register
from hlava.utils import ensure_directory_existence
from ..file import FileItem
import subprocess

@register
class IPythonNotebookItem(FileItem):
    extension = "ipynb"

    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext == IPythonNotebookItem.extension:
                return True
        return False

    def _create(self):
        cache_path = os.path.join(self.storage.root_path, "_cache", "notebooks")
        file_name = self.name + ".html"
        output_path = os.path.join(cache_path, file_name)
        ensure_directory_existence(cache_path)
        args = ["ipython", "nbconvert", "--to", "html", "--output", output_path, self.path]
        subprocess.call(args)    
        return output_path

    def _repr_show_url_(self):
        return self._create()

    def _repr_html_(self):
        path = self._create()
        return "<iframe src='%s'></iframe>" % path