import os
import logging

logging.getLogger("MARKDOWN").setLevel(logging.WARNING)

import markdown

from .. import register
from .text import TextItem


@register
class MarkdownItem(TextItem):
    extension = "md"

    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext == MarkdownItem.extension:
                return True
        return False

    def _repr_html_(self):
        # return mistune.markdown(self.text)
        return markdown.markdown(self.text, extensions=['markdown.extensions.tables',
                                                        'markdown.extensions.codehilite',
                                                        'markdown.extensions.fenced_code',
                                                        'markdown.extensions.wikilinks',
                                                        'markdown.extensions.def_list'])

    @property
    def mime_type(self):
        return "text/markdown"