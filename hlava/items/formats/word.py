from ..file import FileItem
from .. import register
import os
import docx


@register
class WordItem(FileItem):
    @classmethod
    def applies(cls, path):
        if os.path.isfile(path):
            ext = os.path.splitext(path)[1]
            if ext:
                ext = ext[1:]
            if ext in ["docx", "docm"]:
                return True
        return False

    def _repr_html_(self):
        html = u""
        with open(self.path) as f:
            document = docx.Document(f)
            for par in document.paragraphs:
                par_style = ""
                font = par.style.font
                if font:
                    if font.size:
                        style += "font-size: %fpt;" % font.size.pt
                html += "<p style=\"%s\">" % par_style

                # html += "<i>" + par.style.name + "</i>"
                for run in par.runs:
                    style = u""
                    if run.font:
                        if run.font.size:
                            style += "font-size: %fpt;" % run.font.size.pt
                        if run.font.bold:
                            style += "font-weight: bold;"
                        if run.font.name:
                            style += "font-family: '%s'" % run.font.name
                    html += run.text
                    if style:
                        html += "<span class='word-run' style=\"%s\">" % style
                    else:
                        html += "<span class='word-run'>"
                    html += "</span>"
                html += "</p>"
        return html