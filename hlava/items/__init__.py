import os
import glob
import importlib
import logging

logger = logging.getLogger(__name__)

class UnknownFormatError(RuntimeError):
    pass


item_classes = []


def find_constructor(path):
    matches = { float(klass.applies(path)) : klass for klass in item_classes }
    if matches:
        best_match = sorted(matches.keys())[-1]
        if best_match:
            return matches[best_match]
    return None


def read_item(name, storage, path):
    """Try to create item using one of the registered constructors."""
    klass = find_constructor(path)
    if klass:
        return klass(name, storage, path)
    else:
        raise UnknownFormatError("Cannot interpret file %s" % path)


def register(klass):
    """Make this class available to storage for creating items.

    :type klass: type

    Works as decorator for both classes and functions.
    """
    global item_classes
    item_classes.append(klass)
    logger.info("Registered item class %s." % klass.__name__)
    return klass


def _auto_import():
    """Automatically imports all modules from this directory."""
    py_files = glob.glob(os.path.join(os.path.dirname(__file__), "formats") +"/*.py")
    py_files = filter(lambda f: os.path.isfile(f) and not os.path.basename(f).startswith('_'),
                      py_files)
    modules = [ ".formats." + os.path.basename(py_file[:-3]) for py_file in py_files ]

    for module_name in modules:
        try:
            importlib.import_module(module_name, __package__)
        except ImportError as i:
            logger.warning("Module %s could not be imported, reason: %s" % (module_name, i))
    from . import directory

_auto_import()


