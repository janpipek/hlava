from PyQt4 import QtGui

import logging
logger = logging.getLogger(__name__)

try:
    logging.getLogger("pyqode").setLevel(logging.ERROR)
    logging.getLogger("qtpy").setLevel(logging.ERROR)
    from pyqode.core.widgets import GenericCodeEdit
    from pyqode.core.modes import PygmentsSyntaxHighlighter

    class EditWidget(GenericCodeEdit):
        logger.debug("Using PyQode code edit widget.")

        def __init__(self, item, parent=None):
            logger.debug("Edit widget initialized for page %s" % item.name)
            super(EditWidget, self).__init__(parent)
            mime_type = item.mime_type or 'text/plain'
            self.setPlainText(item.text, mime_type, 'utf-8')
            self.setLineWrapMode(QtGui.QPlainTextEdit.WidgetWidth)
            syntax_highlighter = self.modes.get(PygmentsSyntaxHighlighter)
            # syntax_highlighter.pygments_style = 'monokai'
            syntax_highlighter.set_lexer_from_filename(item.path)
except:
    logger.debug("PyQode not found, using basic QTextEdit widget.")

    class EditWidget(QtGui.QTextEdit):
        def __init__(self, item, parent=None):
            super(EditWidget, self).__init__(parent)
            self.setPlainText(item.text)


### spyderlib alternative

        # try:
        #     from spyderlib.widgets.sourcecode.base import TextEditBaseWidget
        #     self.edit_widget = TextEditBaseWidget()
        #     self.edit_widget.set_text(self.item.text)
        #     self.edit_widget.set_linenumberarea_enabled(True)
        #     self.edit_widget.set_language("markdown")