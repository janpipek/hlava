from PyQt4 import QtGui, QtWebKit

from ..config import get_user_or_default_config_path

from pygments import highlight
from pygments.lexers import HtmlLexer
from pygments.formatters import HtmlFormatter

import lxml.html
import lxml.etree

class SourceDialog(QtGui.QDialog):
    '''Dialog displaying HTML source of the view.'''
    def __init__(self, html, title, parent=None):
        super(SourceDialog, self).__init__(parent)
        self.text_edit = QtWebKit.QWebView()
        r = lxml.html.fromstring(html)
        html = lxml.etree.tostring(r, encoding='unicode', pretty_print=True)
        html = highlight(html, HtmlLexer(), HtmlFormatter())
        html += "<style>" + SourceDialog.get_stylesheet() + "</style>"

        self.text_edit.setHtml(html)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.text_edit)
        self.setLayout(layout)
        self.setWindowTitle(title)

        self.setStyleSheet(SourceDialog.get_stylesheet())
        self.adjustSize()

    @classmethod
    def get_stylesheet(cls):
        if not hasattr(cls, "_stylesheet"):
            path = get_user_or_default_config_path("source_view.css")
            with open(path, "r") as f:
                cls._stylesheet = f.read()
        return cls._stylesheet