from PyQt4 import QtGui


class LocationWidget(QtGui.QLineEdit):
    class Completer(QtGui.QCompleter):
        def __init__(self, storage):
            super(LocationWidget.Completer, self).__init__()
            self.storage = storage
            self.setModel(QtGui.QStringListModel())

        def splitPath(self, path):
            if len(str(path)) > 2:
                self.model().setStringList(self.storage.fuzzy_find_names(unicode(path)))
            else:
                self.model().setStringList([])
            return ""

    def __init__(self, storage, parent=None):
        super(LocationWidget, self).__init__(parent)
        self.completer = LocationWidget.Completer(storage)
        self.setCompleter(self.completer)