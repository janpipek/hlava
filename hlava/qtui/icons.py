from PyQt4 import QtCore, QtGui, QtWebKit
import resources_rc

main_icon = QtGui.QIcon()
main_icon.addFile(':images/head-icon16.png', QtCore.QSize(16,16))
main_icon.addFile(':images/head-icon32.png', QtCore.QSize(32,32))