from PyQt4 import QtCore, QtGui
from ..version import __name__, __author__, __version__
import sys

import logging


def run_app():
    logging.getLogger("").setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)

    QtCore.QCoreApplication.setApplicationName(__name__)
    QtCore.QCoreApplication.setOrganizationName(__author__)
    QtCore.QCoreApplication.setApplicationVersion(__version__)

    from .main_window import MainWindow
    main_window = MainWindow()
    main_window.show()

    sys.exit(app.exec_())
