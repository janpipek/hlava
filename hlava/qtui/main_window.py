import webbrowser
import os

from PyQt4 import QtCore, QtGui, QtWebKit

from ..config import get_root_path, get_user_or_default_config_path
from ..filestorage import FileStorage, PageNotFoundError
from .. import operations
from .edit_widget import EditWidget
from .location_widget import LocationWidget
from .source_dialog import SourceDialog
from .icons import main_icon
import qtawesome as qta

import logging
logger = logging.getLogger(__name__)


class MainWindow(QtGui.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.storage = FileStorage(get_root_path(**kwargs))

        self.setWindowTitle("Hlava")
        self.setWindowIcon(main_icon)

        self.build_toolbar()
        self.build_menu()
        self.zoom_factor = 1.0

        self.history = History()
        self.show_item(self.storage.get_root_item())
        self.build_tray_icon()

        self.load_css()
        self.location_text.setFocus()

    def build_toolbar(self):
        self.location_toolbar = self.addToolBar("Location")

        self.location_text = LocationWidget(storage=self.storage)

        self.home_button = QtGui.QToolButton()
        self.home_button.setIcon(qta.icon("fa.home"))
        # self.home_button.setIcon(QtGui.QIcon.fromTheme("go-home"))
        self.home_button.clicked.connect(self.home)

        self.up_button = QtGui.QToolButton()
        self.up_button.setIcon(qta.icon("fa.arrow-circle-up"))
        self.up_button.clicked.connect(self._on_up_clicked)
        self.up_button.setShortcut(QtCore.Qt.CTRL + QtCore.Qt.Key_Up)


        self.prev_button = QtGui.QToolButton()
        self.prev_button.setIcon(qta.icon("fa.arrow-circle-left"))
        self.next_button = QtGui.QToolButton()
        self.next_button.setIcon(qta.icon("fa.arrow-circle-right"))

        self.go_button = QtGui.QToolButton()
        self.go_button.setIcon(qta.icon("fa.search"))
        self.go_button.clicked.connect(self._on_go_clicked)


        self.edit_button = QtGui.QToolButton()
        self.edit_button.setIcon(qta.icon("fa.edit"))
        # self.edit_button.setIcon(QtGui.QIcon.fromTheme(""))
        self.edit_button.clicked.connect(self.edit)
        self.edit_button.setShortcut(QtCore.Qt.CTRL + QtCore.Qt.Key_E)

        self.save_button = QtGui.QToolButton()
        self.save_button.setIcon(qta.icon("fa.save"))
        self.save_button.clicked.connect(self.save)
        self.save_button.setShortcut(QtCore.Qt.CTRL + QtCore.Qt.Key_S)

        self.prev_button.clicked.connect(self.back)
        self.next_button.clicked.connect(self.forward)

        self.location_text.returnPressed.connect(self.go_button.click)

        self.location_toolbar.addWidget(self.prev_button)
        self.location_toolbar.addWidget(self.next_button)
        self.location_toolbar.addWidget(self.home_button)
        self.location_toolbar.addWidget(self.up_button)
        self.location_toolbar.addWidget(self.location_text)
        self.location_toolbar.addWidget(self.go_button)
        self.location_toolbar.addWidget(self.edit_button)
        self.location_toolbar.addWidget(self.save_button)

    def build_menu(self):
        self.file_menu = QtGui.QMenu('&File', self)
        self.new_child_action = self.file_menu.addAction('&Add New Child', self.create_new_child, QtCore.Qt.CTRL + QtCore.Qt.Key_N)
        self.file_menu.addAction('&Add New', self.create_new_global, QtCore.Qt.CTRL + QtCore.Qt.SHIFT + QtCore.Qt.Key_N)
        self.file_menu.addAction('&Quit', self.close, QtCore.Qt.CTRL + QtCore.Qt.Key_Q)

        self.menuBar().addMenu(self.file_menu)

        self.view_menu = QtGui.QMenu("&View", self)
        self.refresh_action = self.view_menu.addAction('&Refresh', self.refresh, QtCore.Qt.CTRL + QtCore.Qt.Key_R)
        self.zoom_in_action = self.view_menu.addAction('Zoom &In', self.zoom_in, QtCore.Qt.CTRL + QtCore.Qt.Key_Plus)
        self.zoom_out_action = self.view_menu.addAction('Zoom &Out', self.zoom_out, QtCore.Qt.CTRL + QtCore.Qt.Key_Minus)

        self.menuBar().addMenu(self.view_menu)

        self.page_menu = QtGui.QMenu("&Page", self)
        self.page_menu.addAction("&Open Externally", self.open_external, QtCore.Qt.CTRL + QtCore.Qt.Key_T)
        self.page_menu.addAction("&Delete", self.delete, QtCore.Qt.CTRL + QtCore.Qt.Key_Delete)

        self.menuBar().addMenu(self.page_menu)

        self.debug_menu = QtGui.QMenu("&Debug", self)
        self.show_source_action = self.debug_menu.addAction("&Show Source", self.show_source)

        self.menuBar().addMenu(self.debug_menu)

    def build_tray_icon(self):
        if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
            self.tray_icon = None
        else:
            self.tray_icon = TrayIcon(self)
            self.tray_icon.show()

    def load_css(self):
        css_path =  get_user_or_default_config_path("ui.css")
        if os.path.exists(css_path):
            with open(css_path, "r") as f:
                self.setStyleSheet(f.read())

    def _on_go_clicked(self):
        name = str(self.location_text.text())
        item = self.storage.get_item(name)
        self.show_item(item)

    def _on_up_clicked(self):
        name = self.item.parent_name
        if name:
            item = self.storage.get_item(name)
            self.show_item(item)
        else:
            self.home()

    def local_navigate(self, name):
        """Navigate to a named local page."""
        try:
            item = self.storage.get_item(name)
            self.show_item(item)
        except PageNotFoundError as er:
            msg = "Page '%s' does not exist. Create?"
            res = QtGui.QMessageBox.question(self, 'Page does not exist.', msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
            if res == QtGui.QMessageBox.Yes:
                item = self.storage.create_item(str(name), "md")
                self.edit(item=item)

    def navigate(self, url):
        """Navigate to URL in response to click in the webview.

        :type urL: QtCore.QUrl
        :param url:
        :return:
        """
        if url.scheme() == "hlava":
            name = str(url.host())
            self.local_navigate(name)
        elif url.isRelative():
            name = str(url.toString())
            self.local_navigate(name)
        else:
            webbrowser.open(url.toString())

    def open_external(self):
        webbrowser.open(self.item.path)

    def show_item(self, item):
        self.history.go(item.name)
        logger.debug("Showing %s" % item.name)
        self.webview = QtWebKit.QWebView()
        self.webview.setZoomFactor(self.zoom_factor)
        self.webview.linkClicked.connect(self.navigate)
        self.webview.page().setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)

        self.setCentralWidget(self.webview)

        self.set_item(item)
        if "_repr_show_url_" in dir(item):
            url = QtCore.QUrl(item._repr_show_url_())
            print url
            self.webview.setUrl(url)
        else:
            html = item._repr_html_()
            self.webview.setHtml(html)
            settings = self.webview.settings()
            settings.setUserStyleSheetUrl(QtCore.QUrl.fromLocalFile(get_user_or_default_config_path("webview.css")))            
        self.setCentralWidget(self.webview)

        self.save_button.setEnabled(False)
        self.edit_button.setEnabled(item.editable)
        self.prev_button.setEnabled(self.history.back_enabled)
        self.next_button.setEnabled(self.history.forward_enabled)
        self.location_text.setFocus()

    def show_source(self):
        html = unicode(self.webview.page().mainFrame().toHtml())
        title = "Source of '%s'" % self.item.name
        dialog = SourceDialog(html, title, self)
        dialog.show()

    def refresh(self):
        self.show_item(self.storage.get_item(self.item.name))

    def delete(self):
        confirm = QtGui.QMessageBox.question(self, "Are you sure:",
                                             "Do you want to remove page %s?" % self.item.name,
                                             QtGui.QMessageBox.Yes | QtGui.QMessageBox.No | QtGui.QMessageBox.Cancel)
        if confirm == QtGui.QMessageBox.Yes:
            parent_name = self.item.parent_name
            operation = operations.Remove(self.storage, self.item.name)
            operation.execute()
            self.local_navigate(parent_name)

    def set_item(self, item):
        self.item = item
        self.location_text.setText(item.name)
        self.new_child_action.setEnabled(True)
        # self.new_child_action.setEnabled(item.has_children())

    def home(self):
        self.show_item(self.storage.get_root_item())

    def edit(self, item=None):
        if item:
            self.set_item(item)
        # except ImportError:
        self.edit_widget = EditWidget(self.item)
        self.setCentralWidget(self.edit_widget)

        self.save_button.setEnabled(True)
        self.edit_button.setEnabled(False)
        self.edit_widget.setFocus()

    def save(self):
        self.item.text = unicode(self.edit_widget.toPlainText())
        self.show_item(self.item)

    def create_new_child(self):
        if not self.item.name:
            self.create_new_global()
        else:
            name, ok = QtGui.QInputDialog.getText(self, 'New page in %s' % self.item.name, 'Enter page name:')
            if ok:
                item = self.item.create_child(str(name), "md")
                # name = self.item._get_child_name(name)
                # item = self.storage.create_item(str(name), "md")
                self.edit(item=item)

    def create_new_global(self):
        name, ok = QtGui.QInputDialog.getText(self, 'New page', 'Enter page name:')
        if ok:
            item = self.storage.create_item(str(name), "md")
            self.edit(item=item)

    def zoom_in(self):
        self.zoom_factor *= 1.1
        self.webview.setZoomFactor(self.zoom_factor)

    def zoom_out(self):
        self.zoom_factor /= 1.1
        self.webview.setZoomFactor(self.zoom_factor)

    def back(self):
        if self.history.back_enabled:
            name = self.history.back()
            self.show_item(self.storage.get_item(name))

    def forward(self):
        if self.history.forward_enabled:
            name = self.history.forward()
            self.show_item(self.storage.get_item(name))


class History(object):
    """Object for storing navigation history."""
    def __init__(self):
        self.current = -1
        self.items = []

    @property
    def current_item(self):
        if not self.items:
            return None
        return self.items[self.current]

    def go(self, item):
        if item == self.current_item:
            return # Do Nothing
        if self.forward_enabled:
            self.items = self.items[:self.current + 1]
        self.items.append(item)
        self.current = len(self.items) - 1

    @property
    def forward_enabled(self):
        return (self.items and self.current < len(self.items) - 1)

    @property
    def back_enabled(self):
        return (self.items and self.current > 0)

    def forward(self):
        if self.forward_enabled:
            self.current += 1
        return self.current_item

    def back(self):
        if self.back_enabled:
            self.current -= 1
        return self.current_item


class TrayIcon(QtGui.QSystemTrayIcon):
    """Tray icon for the application."""
    def __init__(self, parent=None):
        QtGui.QSystemTrayIcon.__init__(self, main_icon, parent)
        # menu = QtGui.QMenu(parent)
        # self.setContextMenu(menu)
        self.activated.connect(self._on_activate)

    def _on_activate(self, reason):
        window = self.parent()
        if reason == QtGui.QSystemTrayIcon.Trigger:
            if window.isVisible():
                window.hide()
                self._state = window.saveState()
                self._geometry = window.saveGeometry()
            else:
                window.show()
                window.restoreGeometry(self._geometry)
                window.restoreState(self._state)