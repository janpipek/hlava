import os
import re
import glob
import logging
import codecs
import blinker
from .utils import ensure_directory_existence


class InvalidNameError(RuntimeError):
    # TODO: Create a useful constructor.
    pass

class PathOutsideRootError(RuntimeError):
    pass

class PageNotFoundError(RuntimeError):
    pass


def valid_ext(ext):
    """Whether the string is a valid extension for file name."""
    return re.match("[a-zA-Z0-9_\-]*$", ext)

def valid_basename(name, enable_meta=False):
    """Whether the string is valid part of the name.

    Generally, it must start with a letter or number..
    Further characters may also include dash and underscore.
    """
    regex = enable_meta and "[a-zA-Z0-9_\-]*$" or "[a-zA-Z0-9][a-zA-Z0-9_\-]*$"
    return re.match(regex, name)


def valid_name(name, enable_meta=False):
    """Whether the string is a valid page name.

    It has to be dot-separated identifier satisfying valid_basename.
    """
    return all(valid_basename(part, enable_meta) for part in name.split("."))

def valid_file_name(name, enable_meta=False):
    """Whether the file name is valid.

    This means it has to be valid_basename + some extension.
    """
    name, ext = os.path.splitext(name)
    return valid_basename(name, enable_meta=enable_meta)


class FileStorage(object):
    """Storage of the system.

    It is based on file system (and the function assume this).
    For a more general solution, it is necessary to implement custom storage class.
    """
    def __init__(self, root_path, versioning=True):
        self.on_operation_executing = blinker.Signal()
        self.on_operation_executed = blinker.Signal()

        self.root_path = root_path
        if versioning:
            from .versioning import Versioning
            self.versioning = Versioning(self)
        else:
            self.versioning = None
        self.root_path = root_path


    def get_item_name(self, path):
        """Returns the name of the item a path would belong to.

        :param path: Path to be interpreted
        :rtype: str

        The path doesn't have to belong to existing item.
        """
        if not os.path.commonprefix([self.root_path, path]) == self.root_path:
            raise PathOutsideRootError()
        relpath = os.path.relpath(path, self.root_path)
        if relpath == ".":
            return ""
        frags = relpath.split(os.path.sep)
        basename = frags[-1]
        if basename.startswith("_"):
            if not valid_basename(os.path.splitext(frags[-1])[0], enable_meta=True):
                raise InvalidNameError()
            frags = frags[:-1]
        else:
            frags[-1] = os.path.splitext(frags[-1])[0]
        if [frag for frag in frags if not valid_basename(frag)]:
            raise InvalidNameError()
        return ".".join(frags)

    def get_item_path(self, name, ext, directory=False):
        """Return the absolute path for an item.

        :param directory: If True, return path of a child item (name/_main)
        :rtype: str

        It is where the item would or should be stored.
        No checking for path existence is performed.
        """
        if not valid_name(name):
            return InvalidNameError(name)
        if not valid_ext(ext):
            return InvalidNameError("Invalid extension")
        frags = name.split(".")
        if directory:
            frags.append("_main")
        if ext:
            frags[-1] += ".%s" % ext
        return os.path.join(self.root_path, *frags)

    def find_item_path(self, name, any_type=False, enable_meta=False):
        """Find absolute path of existing node.

        :param any_type: Include files with extensions we cannot handle.
        """
        from .items import find_constructor

        if not valid_name(name, enable_meta=enable_meta):
            raise InvalidNameError("'%s' is not a valid ID" % name)
        path_candidate = os.path.join(self.root_path, *name.split("."))
        if os.path.isdir(path_candidate):
            return path_candidate
        else:
            dir_name, file_name_stub = os.path.split(path_candidate)
            if not os.path.isdir(dir_name):
                return None
            files = glob.glob(os.path.join(dir_name, "%s.*" % file_name_stub))
            files = sorted(files)
            pattern = "%s\.[^.]+" % file_name_stub
            files = [ f for f in files if re.match(pattern, os.path.basename(f))]
            if files:
                for fname in files:
                    if re.match(pattern, os.path.basename(fname)) and os.path.isfile(fname):
                        if find_constructor(fname):
                            return fname
                if any_type:
                    return files[0]
        return None

    def item_exists(self, name):
        return self.find_item_path(name, any_type=True, enable_meta=True) is not None

    def get_root_item(self):
        from .items import directory
        return directory.RootItem(self)

    def _check_remove_previous(self, name, force=False, **kwargs):
        """Check whether node exists and (possibly) delete it.

        :param force: Enable deleting.

        If force is not specified, exception is thrown.
        """
        existing_dest = self.find_item_path(name, any_type=True, **kwargs)
        if existing_dest:
            if force:
                os.remove(existing_dest)
            else:
                raise OverwriteError("Storing would overwrite existing page.")

    def _find_dest_path(self, name, ext):
        return os.path.join(self.root_path, *name.split(".")) + ext

    def get_item(self, name, throw=True, enable_meta=False, **kwargs):
        """
        :param name:
        :param throw: Whether to raise exception if not found.
        :param kwargs:
        :rtype hlava.items.Item
        :return:
        """
        from .items import read_item

        if name in [".", ""]:
            return self.get_root_item()
        path = self.find_item_path(name, enable_meta=enable_meta)
        if path and os.path.exists(path):
            return read_item(name, self, path)
        elif throw:
            raise PageNotFoundError("Page not found: %s" % name)
        else:
            return None

    def create_item(self, name, ext, content=None, enable_meta=False, **kwargs):
        from .items import read_item

        self._check_remove_previous(name, force=False, enable_meta=enable_meta)
        path = self._find_dest_path(name, ".%s" % ext)
        ensure_directory_existence(os.path.dirname(path))
        # TODO: different formats may require non-empty file
        with codecs.open(path, "w", encoding='utf-8') as f:
            logging.info("New file %s created." % path)
            if content:
                f.write(content)
        if self.versioning:
            self.versioning.item_create(name, path)
        return read_item(name, self, path)

    def __repr__(self):
        return "hlava.Storage('%s')" % self.root_path

    def all_item_names(self):
        """Show all existing names.

        :rtype list of [str]

        Walks the directory structure and includes every valid file
        that we are able to open.
        """
        def find(dirpath, prefix):
            names = []
            for f in os.listdir(dirpath):
                path = os.path.join(dirpath, f)
                if os.path.isdir(path):
                    if valid_basename(f):
                        names.append("%s%s" % (prefix, f))
                        names.extend(find(path, "%s%s." % (prefix, f)))
                elif os.path.isfile(path):
                    if valid_file_name(f):
                        names.append("%s%s" % (prefix, os.path.splitext(f)[0]))
            return names
        return find(self.root_path, "")

    def fuzzy_find_names(self, fragment):
        """Fuzzy find similar to Sublime Text

        :type fragment: str
        :param fragment: Part of the name
        :rtype list of [str]
        """
        # TODO: move to a more appropriate module
        # TODO: optional argument path that takes into account also closeness of paths
        all_names = self.all_item_names()

        matches = []
        if fragment in all_names:
            matches.append(fragment)

        matches.extend((n for n in all_names if n.endswith("." + fragment) and n not in matches))
        matches.extend((n for n in all_names if fragment in n and n not in matches))

        fragment = fragment.replace(".", "\\.")
        re_fuzzy = ".*".join(list(fragment))

        matches.extend((n for n in all_names if re.search(re_fuzzy, n) and n not in matches))

        return matches







