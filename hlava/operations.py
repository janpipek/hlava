import os
import shutil
import codecs
from .utils import ensure_directory_existence
from .filestorage import PageNotFoundError
import StringIO # Used because of unicode support

import logging
logger = logging.getLogger(__name__)


class OverwriteError(RuntimeError):
    pass


class InvalidSourceError(RuntimeError):
    pass


class Operation(object):
    """A modifying operation to be executed on storage."""

    def __init__(self, storage):
        self.storage = storage

        self.items_updated = []
        self.items_added = []
        self.items_renamed = {}
        self.items_removed = []

        self.files_updated = []
        self.files_added = []
        self.files_renamed = {}
        self.files_removed = []

        self.enabled = True

    @property
    def short_description(self):
        """One-sentence of what the operation does.

        Used as first line in a commit.
        """
        return "An operation"

    @property
    def full_description(self):
        """A longer description with all changes."""
        desc = StringIO.StringIO(unicode())
        for item in self.items_updated:
            desc.write(u"Changed: %s" % item)
        for item in self.items_removed:
            desc.write(u"Removed: %s" % item)
        for item in self.items_added:
            desc.write(u"Added: %s" % item)
        for old, new in self.items_renamed.items():
            desc.write(u"Renamed: %s -> %s" % (old, new))
        return desc.getvalue()

    # TODO: Properties all_files_added, all_items_removed, ...?

    def execute(self):
        logger.info(u"Executing main operation: %s" % self.short_description)
        self.storage.on_operation_executing.send(self.storage, operation=self)
        if self.enabled:
            self._do_it()
            self.storage.on_operation_executed.send(self.storage, operation=self)

    def run_child_operation(self, child):
        """

        :param child:
        :type child: Operation
        :return:
        """
        logger.debug(u"Executing child operation %s" % child.short_description)
        child._do_it()

        self.items_updated.extend(child.items_updated)
        self.items_added.extend(child.items_added)
        self.items_renamed.update(child.items_renamed)
        self.items_removed.extend(child.items_removed)

        self.files_updated.extend(child.files_updated)
        self.files_added.extend(child.files_added)
        self.files_renamed.update(child.files_renamed)
        self.files_removed.extend(child.files_removed)

    def _write(self, path, content, encoding='utf-8'):
        if isinstance(content, bytes):
            with open(path, mode="wb") as f:
                f.write(content)
        else:
            with codecs.open(path, mode="w", encoding=encoding) as f:
                f.write(content)

    # TODO: get real directory path for directories


class Update(Operation):
    def __init__(self, storage, item_name, extension, content, encoding='utf-8'):
        """

        :type storage: hlava.filestorage.FileStorage
        :param storage:
        :param item_name:
        :param extension:
        :param content:
        """
        super(Update, self).__init__(storage)
        self.item_name = item_name
        self.path = storage.get_item_path(item_name, extension)
        self.extension = extension
        self.content = content
        self.encoding = encoding

    @property
    def short_description(self):
        return u"Update %s" % self.item_name

    def _do_it(self):
        previous_path = self.storage.find_item_path(self.item_name, any_type=True, enable_meta=True)
        if previous_path != self.path:
            raise PageNotFoundError("Cannot update, page not found.")
        self._write(self.path, self.content, self.encoding)
        self.files_updated.append(self.path)
        self.items_updated.append(self.item_name)


class Create(Operation):
    def __init__(self, storage, item_name, extension, content, encoding='utf-8'):
        super(Create, self).__init__(storage)
        self.item_name = item_name
        self.path = self.storage.get_item_path(item_name, extension)
        self.content = content
        self.encoding = encoding

    @property
    def short_description(self):
        return u"Add %s" % self.item_name

    def _do_it(self):
        if self.storage.item_exists(self.item_name):
            raise OverwriteError()
        # TODO: directorize...
        self._write(self.path, self.content, self.encoding)
        self.items_added.append(self.item_name)
        self.files_added.append(self.path)


class Append(Operation):
    # TODO: Support binary?
    def __init__(self, storage, item_name, content, encoding='utf8', create=True, extension="md"):
        super(Append, self).__init__(storage)
        self.item_name = item_name
        self.content = content
        self.encoding = encoding
        self.create = create
        self.extension = extension

    @property
    def short_description(self):
        return u"Appended to %s" % self.item_name

    def _do_it(self):
        path = self.storage.find_item_path(self.item_name) # For dir, we need the final leaf
        if not path:
            if self.create:
                path = self.storage.get_item_path(self.item_name, self.extension)

                # Delegate to operation that does it well
                op = Create(self.storage, self.item_name, self.extension, content=self.content, encoding=self.encoding)
                self.run_child_operation(op)
            else:
                raise PageNotFoundError("Cannot append to item. It does not exist: %s" % self.item_name)
        else:
            if os.path.isdir(path):
                raise NotImplementedError()
            with codecs.open(path, mode='a', encoding=self.encoding) as f:
                f.write(self.content)
            self.items_updated.append(self.item_name)
            self.files_updated.append(path)


class Rename(Operation):
    def __init__(self, storage, old_name, new_name):
        super(Rename, self).__init__(storage)
        self.old_name = old_name
        self.new_name = new_name

    @property
    def short_description(self):
        return u"Rename %s to %s" % (self.old_name, self.new_name)

    def _do_it(self):
        old_path = self.storage.find_item_path(self.old_name)
        if not old_path:
            raise PageNotFoundError("Cannot rename. Page not found.")
        if self.storage.item_exists(self.new_name):
            raise OverwriteError()
        isdir = os.path.isdir(old_path)
        ext = os.path.splitext(old_path)[1]
        if ext:
            ext = ext[1:]
        new_path = self.storage.get_item_path(self.new_name, ext)
        shutil.move(old_path, new_path)
        self.items_renamed[self.old_name] = self.new_name
        self.files_renamed[old_path] = new_path


class Remove(Operation):
    def __init__(self, storage, item_name):
        super(Remove, self).__init__(storage)
        self.item_name = item_name

    @property
    def short_description(self):
        return u"Remove %s" % self.item_name

    def _do_it(self):
        path = self.storage.find_item_path(self.item_name, any_type=True)
        if os.path.isdir(path):
            if os.listdir(path):
                raise NotImplementedError()
                # TODO: enable deleting directories with _main.md etc.
            os.rmdir(path)
        else:
            os.remove(path)
        self.items_removed.append(self.item_name)
        self.files_removed.append(path)


class Import(Operation):
    """Store a (copy of) file in the database."""

    def __init__(self, storage, item_name, source_path, move=False, force=False):
        """
        :param item_name: Id under which to store
        :param source_path: Path to the file.
        :param move: Whether move the file (otherwise copy is made)
        :param force: Whether to overwrite any previous
        """

        super(Import, self).__init__(storage)
        self.item_name = item_name
        self.source_path = source_path
        self.move = move
        self.force = force

        ext = os.path.splitext(self.source_path)[1]
        if ext:
            ext = ext[1:]
        self.item_path = self.storage.get_item_path(self.item_name, ext)

    def short_description(self):
        return u"Imported %s" % self.item_name

    def _do_shell_op(self):
        ensure_directory_existence(os.path.dirname(self.item_path))
        if self.move:
            shutil.move(self.source_path, self.item_path)
        else:
            shutil.copy(self.source_path, self.item_path)

    def _do_it(self):
        if not os.path.isfile(self.source_path):
            raise InvalidSourceError("The path is not a valid file: %s" % self.source_path)

        current_path = self.storage.find_item_path(self.item_name, any_type=True)

        if current_path:
            if self.force:
                os.remove(current_path)
                self._do_shell_op()
                self.items_updated.append(self.item_name)
                if current_path == self.item_path:
                    self.files_updated.append(self.item_path)
                else:
                    self.files_removed.append(current_path)
                    self.files_added.append(self.item_path)
            else:
                raise OverwriteError("Cannot store item. Would overwrite existing one: %s" % self.item_name)
        else:
            self._do_shell_op()
            self.items_added.append(self.item_name)
            self.files_added.append(self.item_path)


class Link(Operation):
    def __init__(self, storage, target_name, link_name, check_existence=True):
        super(Link, self).__init__(storage)
        self.target_name = target_name
        self.link_name = link_name
        self.check_existence = check_existence

    @property
    def short_description(self):
        return u"Linked %s := %s" % (self.link_name, self.target_name)

    def _do_it(self):
        previous_path = self.storage.find_item_path(self.link_name, any_type=True, enable_meta=True)
        if previous_path:
            raise RuntimeError("TODO: Change to better exception")
        if self.check_existence:
            target = self.storage.find_item_path(self.target_name)
            if not target:
                raise PageNotFoundError("Cannot link. Page dose not exist: %s" % self.target_name)
        link_path = self.storage.get_item_path(self.link_name, "link")
        with open(link_path, "w") as f:
            f.write(self.target_name)
        self.files_added.append(link_path)
        # TODO: inform about link, but how?


class FileToDirectory(Operation):
    def __init__(self, storage, item_name, path):
        super(FileToDirectory, self).__init__(storage)
        self.item_name = item_name
        self.path = path

    @property
    def short_description(self):
        return u"Changed to directory: %s" % self.item_name

    def _do_it(self):
        dir_path = self.storage.get_item_path(self.item_name, ext=None)
        ext = os.path.splitext(self.path)[1]
        if ext:
            ext = ext[1:]
        ensure_directory_existence(dir_path)
        new_path = self.storage.get_item_path(self.item_name, ext=ext)
        shutil.move(self.path, new_path)
        self.files_renamed[self.path] = new_path
