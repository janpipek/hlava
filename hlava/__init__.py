import logging

from .filestorage import FileStorage
from .version import __author__, __version__, __versioninfo__, __name__

from . import config

try:
    import coloredlogs
    coloredlogs.install(level=logging.DEBUG)
except ImportError:
    pass