# import ConfigParser as configparser
from six.moves import configparser
import appdirs
import logging.config
import logging
import os
from .version import __name__, __author__


def get_user_config_path(filename):
    return os.path.join(appdirs.user_config_dir(__name__, __author__, roaming=True), filename)


def get_default_config_path(filename):
    dirname = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(dirname, "_config", filename)


def get_user_or_default_config_path(filename):
    user = get_user_config_path(filename)
    if os.path.exists(user) and os.path.isfile(user):
        return user
    else:
        return get_default_config_path(filename)


def get_root_path(**kwargs):
    """Root directory of data files."""
    if kwargs.get("root_path"):
        return kwargs["root_path"]
    elif "HLAVA_ROOT_PATH" in os.environ:
        return os.environ["HLAVA_ROOT_PATH"]
    else:
        if config.has_option("core", "root_path"):
            return config.get("core", "root_path")
        else:
            return "."


# Logging configuration
# logging_config_file_path = get_user_config_path("logging.ini")
#
# default_logging_options = {
#     "version" : 1,
#     'disable_existing_loggers': True,
#     "handlers": {
#         "console": {
#             "class": "logging.StreamHandler",
#             "level": "INFO"
#         }
#     },
#     "loggers": {
#         "": {
#             "level": "WARNING"
#         }
#     }
# }
#
#
# logging.config.dictConfig(default_logging_options)
#
# try:
#     logging.config.fileConfig(logging_config_file_path)
# except configparser.NoSectionError:
#     logging.debug("No logging config found.")


config_file_paths = [get_default_config_path("settings.ini"), get_user_config_path("settings.ini") ]
config = None

def load_config():
    """(Re-)read config."""
    global config
    parser = configparser.ConfigParser()
    parser.read(config_file_paths)
    config = parser

load_config()






