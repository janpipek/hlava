from . import operations


class Inbox(object):
    """Unsorted place to quickly file info for future processing.

    :type storage: hlava.FileStorage
    """
    def __init__(self, storage):
        self.storage = storage

    def _create_name(self):
        import datetime
        return datetime.date.today().isoformat()

    def store(self, content, name=None, source_path=None, **kwargs):
        if source_path:
            raise NotImplementedError()
        else:
            if not name:
                name = self._create_name()
            name = "inbox.%s" % name

            op = operations.Append(self.storage, name, content, extension="md")
            op.execute()