from . import commands
from collections import OrderedDict
import argparse
import sys
import inspect
from ..config import config, get_root_path


class UnknownCommandError(RuntimeError):
    pass


class _NoCommandError(RuntimeError):
    pass


class _ThrowingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise _NoCommandError()


def console_app():
    """The main function of the console application.

    `hlava` command (created by setuptools) calls it.
    """
    try:
        run_app(*sys.argv[1:])
    except UnknownCommandError as err:
        print(err)


def run_app(*args):
    """Start the command line application.

    :param args: Arguments excluding the name of the script.
    """
    available_command_names = [cmd for cmd in dir(commands) if not cmd.startswith("_")]
    available_commands = OrderedDict((cmd, getattr(commands, cmd)) for cmd in available_command_names)
    available_commands = OrderedDict((cmd_name, cmd) for cmd_name, cmd in available_commands.items()
                                     if callable(cmd) and not inspect.isclass(cmd))

    # Parse available arguments
    parser = _ThrowingArgumentParser(add_help=False)
    parser.add_argument('command_name')
    parser.add_argument('-r', '--root-path')

    try:
        parsed_args, args = parser.parse_known_args(args)
    except _NoCommandError:
        show_usage(available_commands.items())
        sys.exit(0)

    # Get root path of the storage
    kwargs = vars(parsed_args)

    kwargs["root_path"] = get_root_path(**kwargs)

    command_name = kwargs.pop("command_name")

    if not command_name:
        show_usage(available_commands.items())
    elif not command_name in available_commands:
        raise UnknownCommandError("Unknown command '%s'. Available commands: %s" % (command_name, ", ".join(available_commands.keys())))
    else:
        available_commands[command_name](*args, **kwargs)


def show_usage(commands):
    print("Available commands:")
    for name, command in commands:
        print("  %s - %s" % (name, (command.__doc__ or "\n").splitlines()[0]))