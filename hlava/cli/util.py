import os
import subprocess
import webbrowser


def write(item):
    """Write item to the console."""
    title = item.title
    print(title)
    print("=" * len(title))
    text = item.text
    if text:
        print(item.text)

    children = item.get_children_names()
    if children:
        print("Sub-items")
        print("---------")
        for ch in children:
            print ch

def write_tree(item, depth=0, level=0):
    """Write item tree to the console.

    :param depth: 1-show only direct children, 2-children of children, ... 0-all levels
    :param level: current level of indentation (allow recursion)
    """
    if level:
        indent = u"|  " * (level - 1) + u"+--"
        name = item.basename
    else:
        indent = u""
        name = item.name
    print "%s%s" % (indent, name)
    if item.has_children():
        if depth and level == depth:
            return
        for child in item.get_children_names():
            write_tree(item.get_child(child), depth, level + 1)


def open_editor(path, gui=False):
    """

    :param path: absolute path to file
    :param gui: if False, try to edit in console
    """
    from ..config import config

    if not gui:
        if config.has_option("core", "editor-console"):
            editor = config.get("core", "editor-console")
        else:
            editor = os.environ.get("EDITOR")
        if editor:
            subprocess.call([editor, path])
    else:
        webbrowser.open(path)