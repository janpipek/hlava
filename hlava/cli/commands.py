"""Available commands for the hlava command-line interface.

All global functions from this module are automatically made available.
"""
import argparse
import sys
import os

from .. import operations
from ..filestorage import FileStorage, PageNotFoundError
from . import util


def show(*args, **kwargs):
    """Show a page."""
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava show")
    parser.add_argument("id")
    parser.add_argument("-n", "--native", dest="native", action="store_true")
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    try:
        if parsed_args.native:
            path = storage.find_item_path(parsed_args.id, any_type=True)
            if path:
                util.open_editor(path, gui=True)
            else:
                raise PageNotFoundError("Page not found: %s" % parsed_args.id)
        else:
            item = storage.get_item(parsed_args.id)
            util.write(item)
    except PageNotFoundError as err:
        print(err)
        sys.exit(-1)


def store(*args, **kwargs):
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava store")
    parser.add_argument("id")
    parser.add_argument("source")
    parser.add_argument("-m" "--move", dest="move", action='store_true')
    parser.add_argument("-f" "--force", dest="force", action='store_true')
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    op = operations.Import(storage, parsed_args.id, parsed_args.source,
                           move=parsed_args.move, force=parsed_args.force)
    op.execute()

    print("Item successfully stored")


def search(*args, **kwargs):
    """Search for a page."""
    pass


def ln(*args, **kwargs):
    """Assign a new name to a page."""
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava ln")
    parser.add_argument("target")
    parser.add_argument("id")
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    op = operations.Link(storage, parsed_args.target, parsed_args.id)
    op.execute()

    print("Item successfully linked")


def mv(*args, **kwargs):
    """Rename item."""
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava mv")
    parser.add_argument("old")
    parser.add_argument("new")
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    op = operations.Rename(storage, parsed_args.old, parsed_args.new)
    op.execute()

    print("Item successfully moved.")


def rm(*args, **kwargs):
    """Remove item."""
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava rm")
    parser.add_argument("path")
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    op = operations.Remove(storage, parsed_args.path)
    op.execute()

    print("Item successfully removed.")


def edit(*args, **kwargs):
    """Open page in editor.

    In Linux, it is the console $EDITOR.
    """
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava edit")
    parser.add_argument("id")
    parser.add_argument("-c", "--create", dest="create", action='store_true', help="Create a new page")
    parser.add_argument("-e", "--extension", dest="extension", type=str, default="md", help="Extension (without dot) to use for creating new page.")
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    
    try:
        if parsed_args.create:
            path = storage.create_item(name=parsed_args.id, ext=parsed_args.extension).path
        else:
            path = storage.find_item_path(parsed_args.id, any_type=True) # Change to get item
            if not path:
                raise PageNotFoundError("Page not found: %s" % parsed_args.id)
            if os.path.isdir(path):
                raise RuntimeError("Editing directories not yet implemented.")
        util.open_editor(path)
    except RuntimeError as err:
        print(err)
        sys.exit(-1)


def inbox(*args, **kwargs):
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava inbox")
    parser.add_argument("-m", "--message", dest="message", type=str, help="Short message to be included in the new page.")
    parsed_args, _ = parser.parse_known_args(args)

    from ..inbox import Inbox
    storage = FileStorage(root_path)
    the_inbox = Inbox(storage)

    if parsed_args.message:
        message = parsed_args.message
        message += "\n\n"
        the_inbox.store(message)
    else:
        raise NotImplementedError()


def config(*args, **kwargs):
    from ..config import config_file_path # TODO: Fix
    path = config_file_path
    if not os.path.exists(path):
        config_dir = os.path.dirname(path)
        if not os.path.exists(config_dir):
            os.makedirs(config_dir)
        if not os.path.isdir(config_dir):
            print("Wrong configuration directory.")
        with open(path, "w") as f:  # "touch" the file
            pass
    util.open_editor(path)


def gui(*args, **kwargs):
    from ..qtui.app import run_app
    run_app()


def tree(*args, **kwargs):
    root_path = kwargs["root_path"]

    parser = argparse.ArgumentParser(prog="hlava tree")
    parser.add_argument("id")
    parser.add_argument("-d", "--depth", dest="depth", type=int, default=None)
    parsed_args, _ = parser.parse_known_args(args)

    storage = FileStorage(root_path)
    try:
        item = storage.get_item(parsed_args.id)
        util.write_tree(item, parsed_args.depth)
    except PageNotFoundError as err:
        print(err)
        sys.exit(-1)


def help(*args, **kwargs):
    """Display help."""
    pass
